// Fill out your copyright notice in the Description page of Project Settings.


#include "TPRingCollision.h"
#include "Components/SphereComponent.h"

#include "TestProject/Ball/TPBall.h"
#include "TestProject/GameMode/TPGameMode.h"

ATPRingCollision::ATPRingCollision()
{
	RootScene = CreateDefaultSubobject<USceneComponent>("Root");
	SetRootComponent(RootScene);

	RingCollison = CreateDefaultSubobject<USphereComponent>("Ring Collision");
	RingCollison->SetupAttachment(GetRootComponent());
	RingCollison->SetSphereRadius(25.f);
	

	
	

}

void ATPRingCollision::BeginPlay()
{
	Super::BeginPlay();
	
	RingCollison->OnComponentBeginOverlap.AddDynamic(this, &ATPRingCollision::OnOverlapSphere);
}

void ATPRingCollision::OnOverlapSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!OtherActor) return;
	ATPBall* BallRef = Cast<ATPBall>(OtherActor);
	if (!BallRef) return;
	ATPGameMode* GameModeRef = Cast<ATPGameMode>(GetWorld()->GetAuthGameMode());
	if (!GameModeRef) return;
	GameModeRef->Goal();
}


