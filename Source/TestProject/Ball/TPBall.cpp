// Fill out your copyright notice in the Description page of Project Settings.


#include "TPBall.h"
#include "Components/StaticMeshComponent.h"


ATPBall::ATPBall()
{
 	
	PrimaryActorTick.bCanEverTick = false;

	BallMesh = CreateDefaultSubobject<UStaticMeshComponent>("Ball Mesh");
	BallMesh->SetupAttachment(GetRootComponent());
	BallMesh->SetSimulatePhysics(true);
	BallMesh->SetCollisionObjectType(ECC_PhysicsBody);
	BallMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	BallMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);

	//SetLifeSpan(15.f);
}



