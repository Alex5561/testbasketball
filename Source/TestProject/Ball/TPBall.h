// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TPBall.generated.h"

UCLASS()
class TESTPROJECT_API ATPBall : public AActor
{
	GENERATED_BODY()
	
public:	
	
	ATPBall();

protected:
	
	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* BallMesh;

public:	
	
	FORCEINLINE UStaticMeshComponent* GetBallMesh() const { return BallMesh; }
	

};
