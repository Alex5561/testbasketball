// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "TPHud.generated.h"

/**
 * 
 */
UCLASS()
class TESTPROJECT_API ATPHud : public AHUD
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere)
		TSubclassOf<class UTPMainWidget> MainWidgetClass;
	UPROPERTY(EditAnywhere)
		TSubclassOf<class UTPEndGameWidget> EndWidgetClass;

	UPROPERTY()
		UTPMainWidget* MainWidget;
	UPROPERTY()
		UTPEndGameWidget* EndWidget;

	virtual void Tick(float DeltaSeconds) override;

	void SetHUDTime();
	void SetPersentStrenght(float Strenght);

	void EndGameUI();

protected:

	virtual void BeginPlay() override;
	UPROPERTY()
	class ATPGameMode* GameModeREF;
	
	
};
