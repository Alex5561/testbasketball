// Fill out your copyright notice in the Description page of Project Settings.


#include "TPHud.h"
#include "TestProject/UI/TPMainWidget.h"
#include "TestProject/UI/TPEndGameWidget.h"
#include "TestProject/GameMode/TPGameMode.h"



void ATPHud::BeginPlay()
{
	Super::BeginPlay();

	if (MainWidgetClass)
	{
		MainWidget = CreateWidget<UTPMainWidget>(GetWorld(), MainWidgetClass);
		if (!MainWidget) return;
		MainWidget->AddToViewport();
	}
		
	if (EndWidgetClass)
	{
		EndWidget = CreateWidget<UTPEndGameWidget>(GetWorld(), EndWidgetClass);
		EndWidget->AddToViewport();
		EndWidget->SetVisibility(ESlateVisibility::Collapsed);
	}
}


void ATPHud::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	SetHUDTime();
}

void ATPHud::SetHUDTime()
{
	GameModeREF = !GameModeREF ? Cast<ATPGameMode>(GetWorld()->GetAuthGameMode()) : GameModeREF;
	if (!GameModeREF || !MainWidget) return;
	MainWidget->SetTimerText(GameModeREF->GetCurrentTime());

}

void ATPHud::SetPersentStrenght(float Strenght)
{
	MainWidget->SetPersentProgressBar(Strenght);
}

void ATPHud::EndGameUI()
{
	MainWidget->SetVisibility(ESlateVisibility::Collapsed);
	EndWidget->SetVisibility(ESlateVisibility::Visible);
	if (!PlayerOwner) return;
	PlayerOwner->SetShowMouseCursor(true);
	PlayerOwner->SetInputMode(FInputModeUIOnly{});
}
