// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TPRingCollision.generated.h"

UCLASS()
class TESTPROJECT_API ATPRingCollision : public AActor
{
	GENERATED_BODY()
	
public:	
	
	ATPRingCollision();

protected:
	
	virtual void BeginPlay() override;
	UPROPERTY(VisibleAnywhere);
	class USceneComponent* RootScene;
	UPROPERTY(EditAnywhere,BlueprintReadOnly);
	class USphereComponent* RingCollison;

	UFUNCTION()
		void OnOverlapSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
