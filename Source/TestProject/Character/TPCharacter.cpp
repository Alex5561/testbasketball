// Fill out your copyright notice in the Description page of Project Settings.


#include "TPCharacter.h"
#include "Camera/CameraComponent.h"
#include "TestProject/Ball/TPBall.h"
#include "TestProject/HUD/TPHud.h"


ATPCharacter::ATPCharacter() :
	bLeftMousePressed(false),
	bOneTouch(false),
	CurrentStrenght(0.f),
	MaxCurrentStrenght(1500.f)
	
{
 	
	PrimaryActorTick.bCanEverTick = true;

	bUseControllerRotationYaw = true;

	FollowCamera = CreateDefaultSubobject<UCameraComponent>("Follow Camera");
	FollowCamera->SetupAttachment(GetRootComponent());
	FollowCamera->SetRelativeLocation(FVector(50.f, 0.f, 70.f));

}

void ATPCharacter::BeginPlay()
{
	Super::BeginPlay();
	PlayerControllerRef = Cast<APlayerController>(Controller);
	if (!PlayerControllerRef) return;
	Hud = Cast<ATPHud>(PlayerControllerRef->GetHUD());
}

void ATPCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (bLeftMousePressed && bOneTouch)
	{
		CurrentStrenght = FMath::Clamp(CurrentStrenght+=3.f, 0.f, MaxCurrentStrenght);
		SetPercentBarStrenght(CurrentStrenght);
	}
}

void ATPCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction(TEXT("LeftM"), IE_Pressed, this, &ATPCharacter::LeftMousePressed);
	PlayerInputComponent->BindAction(TEXT("LeftM"), IE_Released, this, &ATPCharacter::LeftMouseReleased);
	PlayerInputComponent->BindAction(TEXT("Pause"), IE_Pressed, this, &ATPCharacter::PressedPause);

	PlayerInputComponent->BindAxis(TEXT("Turn"), this, &ATPCharacter::Turn);
	PlayerInputComponent->BindAxis(TEXT("UpDown"), this, &ATPCharacter::UpDown);

}






void ATPCharacter::LeftMousePressed()
{
	bLeftMousePressed = true;
	if (bOneTouch)
	{
		
	}

}

void ATPCharacter::LeftMouseReleased()
{
	bLeftMousePressed = false;

	if (!bOneTouch) bOneTouch = true;
	else
	{
		SpawnBall(CurrentStrenght);
		bOneTouch = false;
		CurrentStrenght = 0.f;
		SetPercentBarStrenght(CurrentStrenght);
	}
	
}

void ATPCharacter::PressedPause()
{
	APlayerController* PlayerControllerREF = Cast<APlayerController>(Controller);
	if (!Controller) return;
	Hud = !Hud ? PlayerControllerREF->GetHUD<ATPHud>() : Hud;
	if (!Hud) return;
	Hud->EndGameUI();
}

void ATPCharacter::Turn(float Value)
{
	if (!Controller || !bLeftMousePressed || bOneTouch)return;
	
		AddControllerYawInput(Value);
	
}

void ATPCharacter::UpDown(float Value)
{
	if (!Controller || !bLeftMousePressed || bOneTouch)return;

	AddControllerPitchInput(Value);
}

void ATPCharacter::SpawnBall(float Strenght)
{
	if (!GetWorld() || !SpawnBallClass) return;
	FTransform TransformSpawnBall = FollowCamera->GetComponentTransform();

	ATPBall* BallClass = GetWorld()->SpawnActor<ATPBall>(SpawnBallClass, TransformSpawnBall);
	if (!BallClass) return;
	FVector PhysicsVelocity = GetControlRotation().Vector() * 1500.f; 
	PhysicsVelocity.Z += Strenght;
	BallClass->GetBallMesh()->SetPhysicsLinearVelocity(PhysicsVelocity);

}

void ATPCharacter::SetPercentBarStrenght(float Strenght)
{
	Hud = !Hud ? Cast<ATPHud>(PlayerControllerRef->GetHUD()) : Hud;
	if (!Hud) return;
	Hud->SetPersentStrenght(Strenght / MaxCurrentStrenght);
}


