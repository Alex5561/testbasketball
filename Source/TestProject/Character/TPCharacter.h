// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TPCharacter.generated.h"



UCLASS()
class TESTPROJECT_API ATPCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	
	ATPCharacter();

	virtual void Tick(float DeltaSeconds) override;

protected:
	
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
		class UCameraComponent* FollowCamera;
	UPROPERTY(EditAnywhere)
		TSubclassOf<class ATPBall> SpawnBallClass;
	UPROPERTY()
		class ATPHud* Hud;
	UPROPERTY()
		class APlayerController* PlayerControllerRef;


	void LeftMousePressed();
	void LeftMouseReleased();
	void PressedPause();

	void Turn(float Value);
	void UpDown(float Value);


	bool bLeftMousePressed;

	bool bOneTouch;
	
	float CurrentStrenght;
	UPROPERTY(EditAnywhere)
	float MaxCurrentStrenght;

	void SpawnBall(float Strenght);

	void SetPercentBarStrenght(float Strenght);

	
	
public:	
	
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
