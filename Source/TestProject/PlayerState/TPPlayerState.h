// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "TPPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class TESTPROJECT_API ATPPlayerState : public APlayerState
{
	GENERATED_BODY()

public:

	void SetNumberofPoints(float Point);
	void SetHudEndGame();
protected:

	void SetHUDScore(float ScoreValue);
	
	
};
