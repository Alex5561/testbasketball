// Fill out your copyright notice in the Description page of Project Settings.


#include "TPPlayerState.h"
#include "TestProject/Character/TPCharacter.h"
#include "TestProject/HUD/TPHud.h"
#include "TestProject/UI/TPMainWidget.h"

void ATPPlayerState::SetNumberofPoints(float Point)
{
	SetScore(GetScore() + Point) ;
	SetHUDScore(GetScore());
}

void ATPPlayerState::SetHUDScore(float ScoreValue)
{
	APlayerController* ControllerRef = GetWorld()->GetFirstPlayerController();;
	if (!ControllerRef) return;
	ATPHud* HudRef = ControllerRef->GetHUD<ATPHud>();
	if (!HudRef) return;
	HudRef->MainWidget->SetCurrentCountText(ScoreValue);
}

void ATPPlayerState::SetHudEndGame()
{
	APlayerController* ControllerRef = GetWorld()->GetFirstPlayerController();
	if (!ControllerRef) return;
	ATPHud* HudRef = ControllerRef->GetHUD<ATPHud>();
	if (!HudRef) return;
	HudRef->EndGameUI();
}