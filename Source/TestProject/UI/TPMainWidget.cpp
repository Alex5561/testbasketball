// Fill out your copyright notice in the Description page of Project Settings.


#include "TPMainWidget.h"
#include "Components/TextBlock.h"
#include "Components/ProgressBar.h"



void UTPMainWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();



}

void UTPMainWidget::SetCurrentCountText(float Count)
{
	if (!CurrentCountText) return;
	FString CountText = FString::Printf(TEXT("%d"), static_cast<int>(Count));
	CurrentCountText->SetText(FText::FromString(CountText));
}

void UTPMainWidget::SetTimerText(float Timer)
{
	if (!TimerText) return;
	int32 Minutes = FMath::FloorToInt(Timer / 60);
	int32 Seconds = Timer - Minutes * 60.f;
	FString TimeText = FString::Printf(TEXT("%02d:%02d"), Minutes, Seconds);
	TimerText->SetText(FText::FromString(TimeText));
}

void UTPMainWidget::SetPersentProgressBar(const float Persent)
{
	if (!StrenghtBar) return;
	StrenghtBar->SetPercent(Persent);
}

void UTPMainWidget::InitUI()
{

}
