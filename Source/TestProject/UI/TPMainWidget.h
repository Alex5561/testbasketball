// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TPMainWidget.generated.h"

/**
 * 
 */
UCLASS()
class TESTPROJECT_API UTPMainWidget : public UUserWidget
{
	GENERATED_BODY()


public:

	UPROPERTY(meta = (BidWidget))
		class UTextBlock* CurrentCountText;

	UPROPERTY(meta = (BidWidget))
		 UTextBlock* TimerText;

	UPROPERTY(meta = (BidWidget))
		class UProgressBar* StrenghtBar;

	void SetCurrentCountText(float Count);
	void SetTimerText(float Timer);
	void SetPersentProgressBar(const float Persent);

	void InitUI();

protected:

	virtual void NativeOnInitialized() override;

};
