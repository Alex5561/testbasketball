// Fill out your copyright notice in the Description page of Project Settings.


#include "TPGameMode.h"
#include "TestProject/PlayerState/TPPlayerState.h"
#include "TestProject/Character/TPCharacter.h"
#include "EngineUtils.h"
#include "Engine/TargetPoint.h"


ATPGameMode::ATPGameMode() :
	TimeMatch(3.f)
	
{
	PrimaryActorTick.bCanEverTick = true;
}

void ATPGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (MatchState == MatchState::InProgress)
	{
		CurrentTime = TimeMatch - GetWorld()->GetTimeSeconds();
		if (CurrentTime <= 0.f)
		{
			EndTimeMatch();
		}
	}
	
}

void ATPGameMode::BeginPlay()
{
	Super::BeginPlay();

	TimeMatch *= 60.f;
	if (TargetPoints.IsValidIndex(IndexLocation))
	{
		SetNewLocationPawn(TargetPoints[IndexLocation]);
	}
	
}

void ATPGameMode::StartPlay()
{
	Super::StartPlay();
	GetTargetPoint();
}

void ATPGameMode::EndTimeMatch()
{
	ATPPlayerState* PlayerStateREF = GetWorld()->GetFirstPlayerController()->GetPlayerState<ATPPlayerState>();
	if (!PlayerStateREF) return;
	PlayerStateREF->SetHudEndGame();
}

void ATPGameMode::GetTargetPoint()
{
	for (ATargetPoint* Target : TActorRange<ATargetPoint>(GetWorld()))
	{
		if (!Target) return;
		TargetPoints.Emplace(Target);
	}
}

void ATPGameMode::GetNewLocationPawn()
{
	if (TargetPoints.Num() == 0) return;
	
	int32 IndexPoint = TargetPoints.IndexOfByPredicate([&](ATargetPoint* TargetValue) { return TargetValue !=TargetPoints[IndexLocation] ; });
	IndexLocation = IndexPoint;
	SetNewLocationPawn(TargetPoints[IndexLocation]);
}

void ATPGameMode::SetNewLocationPawn(const ATargetPoint* Target)
{
	ATPCharacter* CharacterREF = Cast<ATPCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if (!CharacterREF) return;
	const FVector NewLocation =Target->GetActorLocation();
	const FRotator NewRotation = Target->GetActorRotation();
	CharacterREF->SetActorLocation(NewLocation);
	CharacterREF->SetActorRotation(NewRotation);
}

void ATPGameMode::Goal()
{
	ATPPlayerState* PlayerStateREF = GetWorld()->GetFirstPlayerController()->GetPlayerState<ATPPlayerState>();
	if (!PlayerStateREF) return;
	PlayerStateREF->SetNumberofPoints(3.f);
	GetNewLocationPawn();
}



