// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "TPGameMode.generated.h"


UCLASS()
class TESTPROJECT_API ATPGameMode : public AGameMode
{
	GENERATED_BODY()

public:

	ATPGameMode();

	virtual void Tick(float DeltaSeconds) override;
	
protected:
	//Minutes
	UPROPERTY(EditDefaultsOnly)
		float TimeMatch;

	float CurrentTime;


	virtual void BeginPlay() override;
	virtual void StartPlay() override;

	void EndTimeMatch();


	UPROPERTY()
	TArray<class ATargetPoint*> TargetPoints;

	void GetTargetPoint();

	void GetNewLocationPawn();

	void SetNewLocationPawn(const ATargetPoint* Target);

	int32 IndexLocation =1;

public:

	FORCEINLINE float GetCurrentTime() const { return CurrentTime; }
	void Goal();
};
